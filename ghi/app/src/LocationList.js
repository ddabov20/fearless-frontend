const LocationList = (props) => {
    const { locations } = props;

    return (
    <div className="container">
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {props.locations.map(location => {
                        return (
                            <tr key={location.href}>
                                <td>{ location.id }</td>
                                <td>{ location.name }</td>
                                <td><img src={ location.picture_url } /></td>
                            </tr>
                        );
                    })}
            </tbody>
        </table>
    </div>
    );
}

export default LocationList;
