import React, { useEffect, useState } from 'react';

function AttendeeForm (props) {
    const [conferences, setConferences] = useState([]);

    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [email, setEmail] = useState('');

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value)
    }

    const [conference, setConference] = useState('');

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value)
    }

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8000/api/conferences/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setConferences(data.conferences)
            }
        }

        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const response = await fetch('http://localhost:8001/api/attendees/', {
            method: 'POST',
            body: JSON.stringify({
                "name": name,
                "conference": conference,
                "email": email,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            const success = document.getElementById('success-message');
            const form = document.getElementById('create-attendee-form')
            form.classList.add('d-none');
            success.classList.remove('d-none');
        }
    }

    return (
        <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="./logo.svg" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className="mb-3">
                  <select onChange={handleConferenceChange} value={conference} name="conference" id="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return(
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} value={name} placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleEmailChange} value={email} placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}

export default AttendeeForm;
