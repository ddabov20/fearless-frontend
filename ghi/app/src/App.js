
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import LocationList from './LocationList';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <>
    <Nav />
      <div className="container">
          {/* <AttendeesList attendees={props.attendees} />
          <LocationForm /> */}
          {/* <ConferenceForm /> */}
          <AttendeeForm />
        </div>
    </>
  );
}

export default App;
