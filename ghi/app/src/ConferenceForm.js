import React, { useEffect, useState } from 'react';

function ConferenceForm (props) {
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [starts, setStarts] = useState('');

    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value)
    }

    const [ends, setEnds] = useState('');

    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value)
    }

    const [description, setDescription] =useState('');

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value)
    }

    const [max_presentations, setMaxPresentations] = useState('');

    const handleMaxPresentationChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value)
    }

    const [max_attendees, setMaxAttendees] = useState('');

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value)
    }

    const [location, setLocation] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8000/api/locations/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations)
            }
        }

        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const response = await fetch('http://localhost:8000/api/conferences/', {
            method: 'POST',
            body: JSON.stringify({
                "name": name,
                "starts": starts,
                "ends": ends,
                "description": description,
                "max_presentations": max_presentations,
                "max_attendees": max_attendees,
                "location": location,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })

        if (response.ok) {
            const data = await response.json();
            console.log(data)

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} className="form-control" id="description" rows="3" name="description" className="form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationChange} value={max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} value={max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                  <option value="">Choose a Location</option>
                  {locations.map(location => {
                    return(
                        <option key={location.href} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
